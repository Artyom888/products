import * as types from './types';
import axios from 'axios';


const getProducts = (data) => {
    return (dispatch) => {
        dispatch({type: types.GET_PRODUCTS_PENDING});
        axios.get(types.API_PRODUCTS_URL)
            .then(function (response) {
                dispatch({type:types.GET_PRODUCTS_SUCCESS,payload:response.data});
            })
            .catch(function (error) {
                dispatch({type:types.GET_PRODUCTS_REJECTED , payload:error});
            });
    };
};

const editModalVisible = (visible) => {
    return (dispatch) => {
        dispatch({type: types.SET_EDIT_MODAL_VISIBLE});
    };
};

const updateProduct = (updatedProduct,id) => {
    return (dispatch) => {
        const config = {
            onUploadProgress: (progressEvent) => {
                let percentCompleted = Math.round( (progressEvent.loaded * 100) / progressEvent.total );
                dispatch({type: types.UPDATE_PRODUCT_PENDING, payload: percentCompleted});
            }
        };
        axios.post(types.API_PRODUCTS_URL + '/' + id , updatedProduct , config)
            .then(function (response) {
                console.log(response.data);
                dispatch({type:types.UPDATE_PRODUCT_SUCCESS,payload:response.data.data});
            })
            .catch(function (error) {
                dispatch({type:types.UPDATE_PRODUCT_REJECTED , payload:error});
            });
    };
};

const deleteProduct = (productId) => {
    return (dispatch) => {
        dispatch({type: types.DELETE_PRODUCT_PENDING});
        axios.delete(types.API_PRODUCTS_URL + '/' + productId)
            .then(function (response) {
                dispatch({type:types.DELETE_PRODUCT_SUCCESS,payload:response.data});
            })
            .catch(function (error) {
                dispatch({type:types.UPDATE_PRODUCT_REJECTED , payload:error});
            });
    };
};



const addProduct = (data) => {
    return (dispatch) => {
        const config = {
            onUploadProgress: (progressEvent) => {
                let percentCompleted = Math.round( (progressEvent.loaded * 100) / progressEvent.total );
                dispatch({type: types.ADD_PRODUCT_PENDING, payload: percentCompleted});
            }
        };
        axios.post(types.API_PRODUCTS_URL, data,config)
            .then(function (response) {
                if(response.data.success ) {
                    dispatch({type:types.ADD_PRODUCT_SUCCESS,payload:JSON.parse(response.data.data) });
                }
            })
            .catch(function (error) {
                dispatch({type:types.ADD_PRODUCT_REJECTED , payload:error});
            });
    };
};

const getTechnologies = (data) => {
    return (dispatch) => {

        axios.get(types.API_PRODUCTS_URL + types.TECH_URL)
            .then(function (response) {
                dispatch({type:types.GET_TECHNOLOGIES_SUCCESS, payload:response.data});
            })
            .catch(function (error) {
                dispatch({type:types.GET_TECHNOLOGIES_REJECTED , payload:error});
            });
    };
};

const exportProduct = (id , type) => {
    return (dispatch) => {
        const data = {id : id , type:type};
        axios.post(types.API_PRODUCT_EXPORT_URL,data)
            .then(function (response) {
                console.log(response.data);
                // let file = new Blob([response.data], {type: 'application/pdf'});
                // let fileURL = URL.createObjectURL(file);
                // window.open(fileURL);
                dispatch({type:types.EXPORT_FILE_SUCCESS, payload:response.data});
            })
            .catch(function (error) {
                dispatch({type:types.EXPORT_FILE_REJECTED , payload:error});
            });
    };
};

export {addProduct,getTechnologies,getProducts,updateProduct,deleteProduct,editModalVisible, exportProduct};