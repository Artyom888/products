import React, { Component } from 'react';
import '../bootstrap/css/bootstrap.min.css';
import '../styles/style.css';
import '../styles/simple-sidebar.css';

import Sidebar from '../components/sidebar';
import AddProductComponent from '../components/addProduct';
import {connect} from 'react-redux';
import { bindActionCreators } from 'redux';
import { addProduct , getTechnologies} from '../actions/products';


class AddProduct extends Component {
    constructor(props){
        super(props);
        this.addProductHandler = this.addProductHandler.bind(this);
    }

    componentWillMount(){
        this.props.Actions.getTechnologies();
    }

    addProductHandler(data){
        this.props.Actions.addProduct(data);
    }


  render() {
      const {Settings} = this.props;
      return <div id="wrapper" className="wrapper">
            <Sidebar />
            <AddProductComponent addProducts={this.addProductHandler}
                                progressing={Settings.progress}
                                technologies={Settings.technologies}
                                formReset={Settings.formReset}
            />
      </div>
  }
}

const mapStateToProps = (state) => ({
    Settings:{
        technologies:state.products.settings.technologies,
        progress:state.products.settings.progress,
        formReset:state.products.settings.formReset
    }
});

const mapDispatchToProps = (dispatch) => ({
    Actions:bindActionCreators({
        addProduct:addProduct,
        getTechnologies:getTechnologies,
    }, dispatch)
});

export default connect(
    mapStateToProps,
    mapDispatchToProps
)(AddProduct);
