import React, { Component } from 'react';
import '../bootstrap/css/bootstrap.min.css';
import '../styles/style.css';
import '../styles/simple-sidebar.css';

import Sidebar from '../components/sidebar';
import ProductsTableComponent from '../components/productsTable';
import {connect} from 'react-redux';
import { bindActionCreators } from 'redux';
import {getProducts , getTechnologies , updateProduct , deleteProduct , editModalVisible ,exportProduct}  from '../actions/products';


class ProductsList extends Component {
    constructor(props){
        super(props);
        this.updateProductHandler = this.updateProductHandler.bind(this);
        this.deleteProductHandler = this.deleteProductHandler.bind(this);
        this.editModalVisibleHandler = this.editModalVisibleHandler.bind(this);
        this.exportProductHandler = this.exportProductHandler.bind(this);
    }

   componentWillMount(){
       this.props.Actions.getProducts();
       this.props.Actions.getTechnologies();

   }

    updateProductHandler(updatedProduct,id){
        this.props.Actions.updateProduct(updatedProduct,id);
   }

    deleteProductHandler(productId){
        this.props.Actions.deleteProduct(productId);

    }

    editModalVisibleHandler(visible){
        this.props.Actions.setEditModalVisible(visible);
    }

    exportProductHandler(id , type){
        this.props.Actions.setEditModalVisible(id , type);
    }

    render() {
        const {Data,Settings} = this.props;
        return <div id="wrapper" className="wrapper">
                <Sidebar />
                <ProductsTableComponent products={Data ? Data : null}
                                        technologies={Settings.technologies}
                                        progressing={Settings.progress}
                                        tableSpinning={Settings.tableSpinning}
                                        modalLoading={Settings.modalLoading}
                                        editModalVisibleResult={Settings.editModalVisible}
                                        updateProduct={this.updateProductHandler}
                                        deleteProduct={this.deleteProductHandler}
                                        setEditModalVisible={this.editModalVisibleHandler}
                                        exportProduct={this.exportProductHandler}
                                        exportFileUrl={Settings.exportFileUrl ? Settings.exportFileUrl:null }
                                        />
        </div>
    }
}

const mapStateToProps = (state) => ({
    Data: state.products.products,
    Settings:{
        technologies:state.products.settings.technologies,
        progress:state.products.settings.progress,
        tableSpinning:state.products.settings.tableSpinning,
        modalLoading:state.products.settings.modalLoading,
        editModalVisible:state.products.settings.editModalVisible,
        exportFileUrl:state.products.settings.exportFileUrl,
    }
});

const mapDispatchToProps = (dispatch) => ({
    Actions:bindActionCreators({
        getProducts:getProducts,
        getTechnologies:getTechnologies,
        updateProduct:updateProduct,
        deleteProduct:deleteProduct,
        setEditModalVisible:editModalVisible,
        exportProduct:exportProduct,
    }, dispatch)
});

export default connect(
    mapStateToProps,
    mapDispatchToProps
)(ProductsList);
