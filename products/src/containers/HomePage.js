import React, { Component } from 'react';
import '../bootstrap/css/bootstrap.min.css';
import '../styles/style.css';
import '../styles/simple-sidebar.css';

import Sidebar from '../components/sidebar';

class HomePage extends Component {
    constructor(props){
        super(props);
    }

    render() {
        return <div id="wrapper" className="wrapper">
            <Sidebar />
        </div>
    }
}

export default HomePage;