import React, { Component } from 'react';
import '../bootstrap/css/bootstrap.min.css';
import '../styles/style.css';
import '../styles/simple-sidebar.css';

import Sidebar from '../components/sidebar';
import {connect} from 'react-redux';
import { bindActionCreators } from 'redux';


class ProductsList extends Component {
    constructor(props){
        super(props);
    }

    render() {
        return <div id="wrapper" className="wrapper">
            <Sidebar />

        </div>
    }
}

const mapStateToProps = (state) => ({

});

const mapDispatchToProps = (dispatch) => ({
    Actions:bindActionCreators({dispatch})
});

export default connect(
    mapStateToProps,
    mapDispatchToProps
)(ProductsList);
