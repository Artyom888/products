import React, { Component } from 'react';
import {Link } from 'react-router-dom';
import logo from '../images/logo.png';


class Sidebar extends Component {
    render() {
        return <div id="sidebar-wrapper" className="sidebar-wrapper">
                    <ul className="sidebar-nav">
                        <li className="sidebar-brand">
                            <a href="/">
                                <img src={logo} alt="Iguan" className="img-fluid logo" />
                            </a>
                        </li>

                        <li> <Link to="/">Home Page</Link> </li>
                        <li> <Link to="/products">Products List</Link> </li>
                        <li> <Link to="/add">Add Your Product</Link> </li>
                        <li>Projects</li>
                        <li><a href="https://asinkey.com/">Asinkey</a></li>
                        <li><a href="#">Amazcoup</a></li>
                        <li><a href="#">CmsLab</a></li>
                        <li><a href="#">Market 24</a></li>
                        <li>Users</li>
                    </ul>
                </div>

    }
}

export default Sidebar;
