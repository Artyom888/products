import React, {Component} from 'react';
import 'antd/dist/antd.css';
import ProductForm from "./productForm";


class AddProductComponent extends Component {
    constructor(props) {
        super(props);
    }


    render() {
        const {progressing, technologies ,addProducts , formReset} = this.props;

        return <ProductForm actionName={ 'addProduct'}
                            progressing={progressing}
                            technologies={technologies}
                            addProducts={addProducts}
                            formReset={formReset}/>
    }

}

export default AddProductComponent;
