import React, { Component } from 'react';
import '../styles/products.css';
import EditModal from './editModal';
import { Table , Button , Icon , Popconfirm} from 'antd';


class ProductsTableComponent extends Component {
    constructor(props) {
        super(props);
        this.state = {
            data:[],
            columns:[],
        };

        this.showModal = this.showModal.bind(this);
        this.onDelete = this.onDelete.bind(this);
        this.handleTableChange = this.handleTableChange.bind(this);
        this.tableColumnsCreate = this.tableColumnsCreate.bind(this);
        this.exportFileHandler = this.exportFileHandler.bind(this);
    }


    tableColumnsCreate(technologies , exportFileUrl) {
        let filterElems = technologies.map((elem) => {
            return {text: elem.name, value: elem.name};
        });
        let columns = [{
            title: 'Title',
            dataIndex: 'title',
            sorter: (a, b) => a.title.length  > b.title.length ,
            defaultSortOrder: 'ascend',
        }, {
            title: 'Technologies',
            dataIndex: 'technologies',
            defaultSortOrder: 'descend',
            filters:filterElems,
        }, {
            title: 'Operation',
            dataIndex: 'operation',
                render: (text, record) => {
                return (
                    <div className="editable-row-operations">
                        {
                            <a onClick={() => this.showModal(record.key)} style={{marginRight: 25}} > Edit</a>
                        }
                        {
                            <Popconfirm title="Sure to delete?" onConfirm={() => this.onDelete(record.key)}>
                                <a >Delete</a>
                            </Popconfirm>
                        }
                    </div>
                );
            },
        },{
            title: 'Export',
            dataIndex: 'export',
            children: [{
                    title: 'PDF',
                    dataIndex: 'pdf',
                    render: (text, record) => {
                        return (
                            <div className="editable-row-operations">
                                {
                                    <Popconfirm title="Sure to export ?" onConfirm={() => this.exportFileHandler(record.key , 'pdf')}>
                                        <a href={'C:\\xampp\\htdocs\\products project\\laravel products\\storage\\app\\public\\tmpPathForPdf/1521877570.pdf'} download><Button type="default" shape="circle" icon="download"  /></a>
                                    </Popconfirm>
                                }
                            </div>
                        );
                    },
                },{
                    title: 'All Product Files',
                    dataIndex: 'all',
                    render: (text, record) => {
                        return (
                            <div className="editable-row-operations">
                                {
                                    <Popconfirm title="Sure to export ?" onConfirm={() => this.exportFileHandler(record.key , 'all')} >
                                        <Button type="default" shape="circle" icon="download"/>
                                    </Popconfirm>
                                }
                            </div>
                        );
                    },
                }
            ],
          }
        ];
        return columns;
    }

    componentWillReceiveProps(nextProps) {
        const data = [];
        if(nextProps.products !== null){
            nextProps.products.map((field) => {
                let newTech = JSON.parse(field.technologies).join();
                data.push({
                    key: field.id,
                    title: field.title,
                    technologies: newTech,
                });
            });
            let stateChange = {data:data, tableCache:data};
            if( this.props.technologies.length !== nextProps.technologies.length ) {
                stateChange['columns'] = this.tableColumnsCreate(nextProps.technologies , nextProps.exportFileUrl);
            }
            this.setState(stateChange);
        }
    }

    componentDidMount(){
       this.setState({columns:this.tableColumnsCreate(this.props.technologies , this.props.exportFileUrl)});
    }

    onDelete = (id) => {
        this.props.deleteProduct(id);
    };

    exportFileHandler(id , type){
        this.props.exportProduct(id , type)
    }

    showModal = (id) => {
        let editField = this.props.products.find((field) => {
            return field.id === id;
        });
        this.setState({
            editField: editField
        });
        this.props.setEditModalVisible(true);
    };


    handleTableChange = (pagination, filters, sorter) => {
        let filterData = filters.technologies;
        if(filterData) {
            let filterState = this.state.tableCache.filter((field, index) => {
                let matches = true;
                for (let i = 0; i < filterData.length; i++) {
                    if (field.technologies.indexOf(filterData[i]) < 0) {
                        matches = false;
                    }
                }
                return matches;
            });
            this.setState({
                data: filterState
            });
        }
    };


    render() {
        const { progressing, technologies,tableSpinning, modalLoading,updateProduct, setEditModalVisible ,editModalVisibleResult} = this.props;
        return <div>
            {
                editModalVisibleResult ?   <EditModal editedProduct = {this.state.editField}
                                                       modalVisible = {editModalVisibleResult}
                                                       progressing = {progressing}
                                                       technologies = {technologies}
                                                       modalLoading = {modalLoading}
                                                       updateProduct = {updateProduct}
                                                       editModalClose = {setEditModalVisible}/>
                    : []
            }

            <Table columns={this.state.columns} bordered loading={tableSpinning} dataSource={this.state.data} onChange={this.handleTableChange} />
        </div>
    }
}

export default ProductsTableComponent;
