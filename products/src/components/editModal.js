import React, { Component } from 'react';
import CKEditor from "react-ckeditor-component";
import '../styles/products.css';
import validator from 'validator';
import { Modal , Upload , Icon ,Button , Select,Progress , message} from 'antd';

class EditModal extends Component {
    constructor(props) {
        super(props);
        this.state = {
            editorCacheData:'',
            descFieldFocus : false,
            accessFieldFocus : false,
            id:'',
            title:'',
            desc:'',
            access:'',
            technologies:'',
            fileList: [],
            Validate:{
                valid:true,
                fields:{
                    title : {
                        message : null,
                        valid : true
                    },
                    desc :  {
                        message : null,
                        valid : true
                    },
                    access : {
                        message : null,
                        valid : true
                    },
                    technologies : {
                        message : null,
                        valid : true
                    },
                    file : {
                        message : null,
                        valid : true
                    }
                },
            },
        };
        this.handleInputChange = this.handleInputChange.bind(this);
        this.handleSelectChange = this.handleSelectChange.bind(this);
        this.handleSubmit = this.handleSubmit.bind(this);
        this.handleCancel = this.handleCancel.bind(this);
        this.validate = this.validate.bind(this);
        this.onTextFocus = this.onTextFocus.bind(this);
        this.onBlurEditor = this.onBlurEditor.bind(this);
        this.onFileRemove = this.onFileRemove.bind(this);
        this.beforeFileUpload = this.beforeFileUpload.bind(this);
    }



    componentWillMount() {
       this.setState({
           id:this.props.editedProduct.id,
           title:this.props.editedProduct.title,
           desc:this.props.editedProduct.description,
           access:this.props.editedProduct.access,
           technologies:JSON.parse(this.props.editedProduct.technologies),
           file:this.props.editedProduct.filename,
       });

    }

    validate(validField,value) {
        const newValidate  = this.state.Validate;
        value = value.trim();
        if(validator.isEmpty(value)){
            newValidate.fields[validField] = {
                valid : false,
                message : `${validField} must not be empty !`
            };
        }else if( value.length > 100 ){
            newValidate.fields[validField] = {
                valid : false,
                message : `${validField} must not be greater than 100 symbols !`
            }
        }else{
            newValidate.fields[validField] = {
                valid : true,
                message : null
            };
        }

        this.setState({ Validate : newValidate },() => {
            let valid = true;
            Object.values(this.state.Validate.fields).map(function(field) {
                if( !field.valid){
                    valid = false ;
                }
            });

            let updateStateValid = this.state.Validate;
            updateStateValid.valid = valid;
            this.setState({ Validate:updateStateValid });
        });
    }

    onTextFocus( event ){
        let fieldFocus = event.target.name + 'FieldFocus';
        this.setState({
            [fieldFocus]:true
        });
    }

    onBlurEditor ( event ,field){
        let fieldFocus = field + 'FieldFocus';
        this.setState({
            [fieldFocus]:false
        });
        this.setState({
            editorTmpData:event.editor.getData()
        });
    }

    handleInputChange(event , editorFieldName) {
        let value;
        let name;
        if(event.target && event.target.name === 'title'){
            this.validate(event.target.name,event.target.value);
            value = event.target.value;
            name = event.target.name;
        }else {
            value = event.editor.getData();
            name = editorFieldName;
        }

        this.setState({ [name] : value });
    }

    handleSelectChange(value){
        const newValidate  = this.state.Validate;
        if(value.length > 0){
            newValidate.fields.technologies = {
                valid:true,
                message:null
            };
            this.setState({technologies: value});
        }else{
            newValidate.fields.technologies = {
                valid:false,
                message:'Field must not be empty! '
            }
        }
        this.setState({ Validate : newValidate },() => {
            let valid = true;
            Object.values(this.state.Validate.fields).map(function(field) {
                if( !field.valid){
                    valid = false ;
                }
            });

            let updateStateValid = this.state.Validate;
            updateStateValid.valid = valid;
            this.setState({ Validate:updateStateValid });
        });
    }

    handleCancel(){
        this.props.editModalClose();
    }


    handleSubmit(event , id) {
        event.preventDefault();
        let formData = new FormData();

        formData.append("title", this.state.title);
        formData.append("desc", this.state.desc);
        formData.append("access", this.state.access);
        formData.append("technologies", JSON.stringify(this.state.technologies));

        for(let i = 0 ; i < this.state.fileList.length ; i++) {
            formData.append('file' + i , this.state.fileList[i]);
        }
        if(this.state.Validate.valid){
            this.props.updateProduct(formData , id);
        }
    }


    beforeFileUpload = (file) => {
        const newValidate  = this.state.Validate;
        if( validator.matches(file.type , / text\/csv|text\/plain|text\/html|application\/pdf|application\/msword|application\/vnd.ms-excel /i)) {
            newValidate.fields.file = {
                valid:true,
                message:null
            };
            this.setState(({ fileList }) => ({
                fileList: [...fileList , file],
            }));
            message.success('selected successfully.');
        }else{
            message.error('upload file format failed.');
            newValidate.fields.file = {
                valid:false,
                message:'The invalid file format or you don\'t select upload file !'
            };
        }

        this.setState({ Validate : newValidate },() => {
            let valid = true;
            Object.values(this.state.Validate.fields).map(function(field) {
                if( !field.valid){
                    valid = false ;
                }
            });

            let updateStateValid = this.state.Validate;
            updateStateValid.valid = valid;
            this.setState({ Validate:updateStateValid });
        });

        return false;
    };

    onFileRemove = (file) => {
        if(this.state.fileList.includes(file)) {
            this.setState(({ fileList }) => {
                const index = fileList.indexOf(file);
                const newFileList = fileList.slice();
                newFileList.splice(index, 1);
                return {
                    fileList: newFileList,
                };
            });
        }
    };

    render() {
        const { progressing, technologies ,modalLoading , modalVisible} = this.props;
        const ValidateField = this.state.Validate.fields;
        const Option = Select.Option;
        const selectChildren = [];
        technologies.map((field) => {
        selectChildren.push(<Option key={field.name}>{field.name}</Option>);
        });
        const fileProps = {
            name:'file',
            onRemove: this.onFileRemove ,
            beforeUpload: this.beforeFileUpload,
            fileList : this.state.fileList,
        };

        console.log( this.state );
        return <div>
            <Modal
                title= 'Edit Product'
                visible={modalVisible}
                okText='Update'
                confirmLoading={modalLoading}
                onOk={(e) => this.handleSubmit(e,this.state.id)}
                onCancel={(e) => this.handleCancel(e)}
            >
                <form className="add-product-form"  >
                    <div className="form-group">
                        <label htmlFor="ProductTitle">Product Title</label>
                        <input type="text" name='title' className="form-control"
                               defaultValue={this.state.title}
                               onChange={(e) => this.handleInputChange(e)}
                               id="ProductTitle"
                               aria-describedby="ProductTitleHelp"
                        />
                        <small id="ProductTitleHelp"
                               className={`form-text text-muted alert alert-${ValidateField.title.message && 'danger'}`}>
                            {ValidateField.title.message}
                        </small>
                    </div>
                    <div className="form-group">
                        <label htmlFor="ProductDescription">Product Description</label>
                        {
                            this.state.descFieldFocus ?  <CKEditor
                                activeClass="ckEditor"
                                content={this.state.editorTmpData}
                                events={{
                                    "change": (e) => this.handleInputChange(e ,'desc'),
                                    "blur": (e) => this.onBlurEditor(e , 'desc'),
                                }}
                            /> :  <textarea className="form-control" name='desc'
                                            id="ProductDescription" rows="2"
                                            onFocus={(e) => this.onTextFocus(e)}
                            />
                        }
                        <small id="ProductDescription"
                               className={`form-text text-muted alert alert-${ValidateField.desc.message && 'danger'}`}>
                            {ValidateField.desc.message}
                        </small>
                    </div>
                    <div className="form-group">
                        <label htmlFor="Access">Access</label>
                        {
                            this.state.accessFieldFocus ? <CKEditor
                                activeClass="ckEditor"
                                content={this.state.editorTmpData}
                                events={{
                                    "change": (e) => this.handleInputChange(e , 'access'),
                                    "blur": (e) => this.onBlurEditor(e , 'access'),
                                }}
                            /> : <textarea className="form-control" name='access'
                                           id="Access" rows="2"
                                           onFocus={(e) => this.onTextFocus(e)}
                            />
                        }
                        <small id="Access"
                               className={`form-text text-muted alert alert-${ValidateField.access.message && 'danger'}`}>
                            {ValidateField.access.message}
                        </small>
                    </div>
                    <div className="form-group">
                        <label htmlFor="multiSelect">Select Technology</label>
                        <Select
                            mode="multiple"
                            style={{ width: '100%' }}
                            placeholder="Please select"
                            allowClear={true}
                            id="multiSelect"
                            name='technologies'
                            defaultValue={
                                this.state.technologies ? this.state.technologies : []
                            }
                            onChange={(e) => this.handleSelectChange(e)}
                        >
                            {selectChildren}
                        </Select>
                        <small id="Access"
                               className={`form-text text-muted alert alert-${ValidateField.technologies.message && 'danger'}`}>
                            {ValidateField.technologies.message}
                        </small>
                    </div>
                    <div className="form-group">
                        <Upload {...fileProps} >
                            <Button >
                                <Icon type="upload" /> upload
                            </Button>
                        </Upload>
                        <small id="fileHelp"
                               className={`form-text text-muted alert alert-${ValidateField.file.message && 'danger'}`}>
                            This is some placeholder block-level help text for
                            the above input. It's a bit lighter and easily wraps to a new line.
                            {ValidateField.file.message}
                        </small>
                        {
                            progressing.visiable && this.state.file ?
                                <Progress percent={progressing.percent} strokeWidth ={10} showInfo={true} format={percent => `${percent} %`}/>
                                : null
                        }
                    </div>
                </form>
            </Modal>
        </div>

    }

}

export default EditModal;
