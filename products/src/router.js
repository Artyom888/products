import React from 'react';
import {BrowserRouter, Route, Switch} from 'react-router-dom';
import { Provider } from 'react-redux';
import store from './store';
import HomePage from './containers/HomePage';
import AddProduct from './containers/AddProduct';
import ProductsList from './containers/ProductsList';
import AdminPanel from './containers/AdminPanel';

const Router = (
  <Provider className="app" store={store}>
    <BrowserRouter>
      <Switch>
        <Route exact path="/admin" component={AdminPanel}/>
        <Route exact path="/" component={HomePage}/>
        <Route exact path="/add" component={AddProduct}/>
        <Route exact path="/products" component={ProductsList}/>
        {/*<Route path="/articles" component={bundle(Articles)}/>*/}
        {/*<Route path="/article/:id" component={bundle(Article)}/>*/}
        {/*<Route path="/about" component={bundle(About)}/>*/}
      </Switch>
    </BrowserRouter>
  </Provider>
);

export default Router;
