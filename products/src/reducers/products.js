import * as types from '../actions/types';

const initialState = {
    products:[],
    error: null,
    settings:{
        technologies:[],
        progress:{ percent:0,visible:false },
        tableSpinning:false,
        editModalVisible:false,
        modalLoading:false,
        formReset:false,
        exportFileUrl:null,
    }
};

export function products(state = initialState, action) {
    switch(action.type) {
    //GET PRODUCTS
        case types.GET_PRODUCTS_PENDING:
            return Object.assign({}, state, {
                settings:{
                    ...state.settings,
                    tableSpinning:true
                },
            });
        case types.GET_PRODUCTS_SUCCESS:
            return Object.assign({}, state, {
                products:action.payload,
                settings:{
                    ...state.settings,
                    tableSpinning:false
                },
            });
        case types.GET_PRODUCTS_REJECTED:
            return Object.assign({}, state, {
                error:action.payload.error,
            });
        //GET TECHNOLOGY
        case types.GET_TECHNOLOGIES_SUCCESS:
            return Object.assign({}, state, {
                settings:{
                    ...state.settings,
                    technologies:action.payload,
                },
            });
        case types.GET_TECHNOLOGIES_REJECTED:
            return Object.assign({}, state, {
                error:action.payload.error,
            });
      //ADD PRODUCT
        case types.ADD_PRODUCT_PENDING:
            return Object.assign({}, state, {
                settings:{
                    ...state.settings,
                    progress:{percent:action.payload,visible:true},
                },
            });
        case types.ADD_PRODUCT_SUCCESS:
            return Object.assign({}, state, {
                products:[...state.products , action.payload],
                settings:{
                    ...state.settings,
                    progress:{percent:100,visible:false},
                    formReset:true,
                },
            });
        case types.ADD_PRODUCT_REJECTED:
            return Object.assign({}, state, {
                error: action.payload
            });
        // EDIT MODAL VISIBLE
        case types.SET_EDIT_MODAL_VISIBLE:
            let visible = !state.settings.editModalVisible;
            return Object.assign({}, state, {
                settings:{
                    ...state.settings,
                    editModalVisible:visible
                },
            });
        // UPDATE PRODUCTS
        case types.UPDATE_PRODUCT_PENDING:
            return Object.assign({}, state, {
                settings:{
                    ...state.settings,
                    modalLoading:true,
                    progress:{percent:action.payload,visible:true},
                },
            });
        case types.UPDATE_PRODUCT_SUCCESS:
            let updateState =  state.products.map( item => {
                if(item.id === action.payload.id){
                    item = action.payload;
                }
                return item;
            });
        return Object.assign({}, state, {
            settings:{
                ...state.settings,
                products:updateState,
                modalLoading:false,
                progress:{percent:100,visible:false},
            },
        });
        case types.UPDATE_PRODUCT_REJECTED:
            return Object.assign({}, state, {
                error: action.payload
            });
        // DELETE PRODUCTS
        case types.DELETE_PRODUCT_PENDING:
            return Object.assign({}, state, {
                settings:{
                    ...state.settings,
                    tableSpinning:true
                },
            });
        case types.DELETE_PRODUCT_SUCCESS:
            let newState =  state.products.filter(item => item.id !== action.payload);
            if(newState.length > 0 ){
                return Object.assign({}, state, {
                    settings:{
                        ...state.settings,
                        products:newState,
                        tableSpinning:false
                    },
                });
            }else{
                return {
                    settings:{
                        ...state.settings,
                        tableSpinning:false,
                    },
                  };
            }
        case types.DELETE_PRODUCT_REJECTED:
            return Object.assign({}, state, {
                error: action.payload
            });
         // EXPORT FILE
        case types.EXPORT_FILE_SUCCESS:
            return Object.assign({}, state, {
                settings:{
                    ...state.settings,
                    exportFileUrl:action.payload
                },
            });
        case types.EXPORT_FILE_REJECTED:
            return Object.assign({}, state, {
                error: action.payload
            });
        default:
            return state;
    }
}


export default products;