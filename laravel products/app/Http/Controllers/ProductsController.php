<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Product;
use App\Technology;
use Carbon\Carbon;
use Illuminate\Support\Facades\Storage;
use Illuminate\Http\File;
use Zipper;
use PDF;

class ProductsController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return Product::all();
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    public function getTech(){
        return Technology::all();
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $response = ['success' => false];
        $messages = [
            'required'=> 'Fill all fields!',
        ];
        $rules = [
            'title' => 'required|max:100' ,
            'technologies' => 'required',
        ];
        $this->validate($request,$rules,$messages);
        $title = $request->input('title');
        $desc = $request->input('desc');
        $access = $request->input('access');
        $technologies = $request->input('technologies');
        $files = $request->file();

        $params = [
            'title' => $title,
            'description' => $desc,
            'access' => $access,
            'technologies' => $technologies,
        ];
            if($files){
                $filenames = [];
                $currentTimestamp = Carbon::now()->timestamp;
                foreach ($files as $file){
                    $save = Storage::putFile('ProductsFiles/'.$currentTimestamp, new File($file));
                    $filenames[] = $save;
                }
                $params['filename'] = json_encode($filenames);
            }
        $addProduct = Product::create($params);
        if($addProduct){
            $response['data'] = json_encode($addProduct);
            $response['success'] = true;
            $response['message'] = 'The product Created';
            return response()->json($response,201);
        }else{
            $response['message'] = 'The product does not Created';
            return response()->json($response,400);
        }

    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Product  $product
     * @return \Illuminate\Http\Response
     */
    public function show(Product $product)
    {
        return $product;

    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Product  $product
     * @return \Illuminate\Http\Response
     */
    public function edit(Product $product)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Product  $product
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request,$id)
    {
        $response = ['success' => false];
        $messages = [
            'required'=> 'Fill all fields!',
        ];
        $rules = [
            'title' => 'required|max:100' ,
            'technologies' => 'required',
        ];
        $this->validate($request,$rules,$messages);
        $title = $request->input('title');
        $desc = $request->input('desc');
        $access = $request->input('access');
        $technologies = $request->input('technologies');
        $files = $request->file();
        $params = [
            'title' => $title,
            'description' => $desc,
            'access' => $access,
            'technologies' => $technologies,
        ];
        if($files){
            $prevProduct = Product::select('filename')->where('id',$id)->first();
            if($prevProduct->filename){
                $prevFiles = json_decode($prevProduct->filename);
                foreach($prevFiles as $prevFile){
                    $prevFilePath = explode("/", $prevFile);
                    Storage::deleteDirectory('ProductsFiles/'. $prevFilePath[1]);
                }
                $filenames = [];
                $currentTimestamp = Carbon::now()->timestamp;
                foreach ($files as $file){
                    $save = Storage::putFile('ProductsFiles/'.$currentTimestamp, new File($file));
                    $filenames[] = $save;
                }
                $params['filename'] = json_encode($filenames);
            }
        }
        $updateProduct = Product::findOrFail($id);
        $updateProduct->update($params);
        if($updateProduct){
            $response['data'] = $updateProduct;
            $response['success'] = true;
            $response['message'] = 'The product Updated';
            return response()->json($response,200);
        }else{
            $response['message'] = 'The product does not Updated';
            return response()->json($response,400);
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Product  $product
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $product = Product::findOrFail($id);
        if($product->filename){
            $prevFiles = json_decode($product->filename);
            foreach($prevFiles as $prevFile){
                $prevFilePath = explode("/", $prevFile);
                Storage::deleteDirectory('ProductsFiles/'. $prevFilePath[1]);
            }
        }
        $product->delete();
        return response()->json($product->id, 200);
    }

    public function export(Request $request){
        $zipper = new \Chumper\Zipper\Zipper;
        $exportProductId = $request->post('id');
        $exportType = $request->post('type');

        if ( $exportType === 'all' ) {
//            Storage::deleteDirectory(storage_path('app/tmpPathForZip'));
            $product = Product::find($exportProductId);
                $files = json_decode($product->filename);
                $getFilesPath = [];
                    foreach($files as $file){
                       $getFilesPath[] = $file;
                    }
                $currentTimestamp = Carbon::now()->timestamp;
                foreach($getFilesPath as $filePath){
                    $zipper->zip(storage_path("app\\tmpPathForZip\\product$currentTimestamp.zip"))->add(storage_path('/app/'.$filePath ));
                }
            return response([storage_path("app\\tmpPathForZip\\product$currentTimestamp.zip") , 'zip']);
        }else if($exportType === 'pdf') {
//                Storage::deleteDirectory(storage_path('app/tmpPathForPdf'));
                $product = Product::find($exportProductId);
                $pdf = PDF::loadView('myPdfFile', compact('product'))->stream();
                $currentTimestamp = Carbon::now()->timestamp;
                $save = Storage::put("public/tmpPathForPdf/$currentTimestamp.pdf", $pdf , 'public');
//                 $fileContent = file_get_contents(storage_path("app/public/tmpPathForPdf/$currentTimestamp.pdf"));
            $fileContent = Storage::get(storage_path("app/public/tmpPathForPdf/$currentTimestamp.pdf"));

            return base64_encode(response()->downlkoad($fileContent));
        }
    }
}
