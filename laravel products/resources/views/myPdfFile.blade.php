<!DOCTYPE html>
<html>
<head>
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css" integrity="sha384-Gn5384xqQ1aoWXA+058RXPxPg6fy4IWvTNh0E263XmFcJlSAwiGgFAW/dAiS6JXm" crossorigin="anonymous">
    <title></title>
</head>
<body>
<div class="container">

    <table class="table table-bordered">
    <thead>
    <tr  class="thead-dark">
        <th>Title</th>
        <th>Description</th>
        <th>Access</th>
        <th>Technologies</th>
    </tr>
    </thead>
    <tbody>

        <tr>
            <td>{{ $product->title  }}</td>
            <td>{{ $product->description  }}</td>
            <td>{{ $product->access  }}</td>
            <td>{{ implode(" , ", json_decode($product->technologies, true)) }}</td>
        </tr>
    </tbody>
</table>
</div>
</body>
</html>