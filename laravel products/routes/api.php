<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::middleware('auth:api')->get('/user', function (Request $request) {
    return $request->user();
});
Route::get('products/tech', 'ProductsController@getTech');

Route::get('products', 'ProductsController@index');
Route::get('products/{products}', 'productsController@show');
Route::post('products', 'productsController@store');
Route::post('products/{products}', 'productsController@update');
Route::delete('products/{products}', 'productsController@destroy');

Route::get('product/export', 'ProductsController@export');
Route::post('product/export', 'ProductsController@export');
