<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class TechnologiesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('technologies')->insert([
            'name' => 'HTML5',
        ]);
        DB::table('technologies')->insert([
            'name' => 'PHP',
        ]);
        DB::table('technologies')->insert([
            'name' => 'laravel ',
        ]);
        DB::table('technologies')->insert([
            'name' => 'Yii2 ',
        ]);
        DB::table('technologies')->insert([
            'name' => 'JavaScript',
        ]);
        DB::table('technologies')->insert([
            'name' => 'ReactJs',
        ]);
        DB::table('technologies')->insert([
            'name' => 'Redux',
        ]);
    }
}
